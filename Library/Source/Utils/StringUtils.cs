using System.Text;

namespace Markwardt.DataObjects
{
    public static class StringUtils
    {
        public static string CreateIndent(int indent)
        {
            if (indent <= 0)
            {
                return string.Empty;
            }

            var builder = new StringBuilder();
            for (int i = 0; i < indent; i++)
            {
                builder.Append('\t');
            }

            return builder.ToString();
        }
    }
}