using System.IO;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public static class ReaderUtils
    {
        public static char? ReadCharacter(this TextReader reader)
        {
            var buffer = new char[1];
            int read = reader.ReadBlock(buffer, 0, buffer.Length);
            
            if (read == 0)
            {
                return null;
            }
            else
            {
                return buffer[0];
            }
        }

        public static async Task<char?> ReadCharacterAsync(this TextReader reader)
        {
            var buffer = new char[1];
            int read = await reader.ReadBlockAsync(buffer, 0, buffer.Length);
            
            if (read == 0)
            {
                return null;
            }
            else
            {
                return buffer[0];
            }
        }
    }
}