using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Markwardt.DataObjects
{
    public static class ReflectionUtils
    {
        public static string GetFriendlyName(this Type type, bool friendlyGenericNames = true)
        {
            if (type == typeof(bool)) { return "bool"; }
            else if (type == typeof(byte)) { return "byte"; }
            else if (type == typeof(sbyte)) { return "sbyte"; }
            else if (type == typeof(short)) { return "short"; }
            else if (type == typeof(ushort)) { return "ushort"; }
            else if (type == typeof(int)) { return "int"; }
            else if (type == typeof(uint)) { return "uint"; }
            else if (type == typeof(long)) { return "long"; }
            else if (type == typeof(ulong)) { return "ulong"; }
            else if (type == typeof(float)) { return "float"; }
            else if (type == typeof(double)) { return "double"; }
            else if (type == typeof(decimal)) { return "decimal"; }
            else if (type == typeof(string)) { return "string"; }

            string prefix = string.Empty;
            if (type.DeclaringType != null)
            {
                prefix = type.DeclaringType.GetFriendlyName() + ".";
            }

            string name = type.Name;
            int genericIndex = type.Name.IndexOf('`');
            if (genericIndex != -1)
            {
                name = type.Name.Substring(0, genericIndex);
            }

            string suffix = string.Empty;
            if (type.IsGenericTypeDefinition)
            {
                suffix = "<";

                int commas = type.GetGenericArguments().Count() - 1;
                for (int i = 0; i < commas; i++)
                {
                    suffix += ",";
                }
                
                suffix += ">";
            }
            else if (type.GenericTypeArguments.Length > 0)
            {
                suffix = "<" + string.Join(", ", type.GenericTypeArguments.Select(t => friendlyGenericNames ? t.GetFriendlyName() : t.GetFullFriendlyName())) + ">";
            }

            return $"{prefix}{name}{suffix}";
        }

        public static string GetFullFriendlyName(this Type type)
            => $"{type.Namespace}.{type.GetFriendlyName(false)}";

        public static IEnumerable<Type> GetAllNestedTypes(this Type type)
        {
            foreach (Type nestedType in type.GetNestedTypes(BindingFlags.Static | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                yield return nestedType;

                foreach (Type deepNestedType in nestedType.GetAllNestedTypes())
                {
                    yield return deepNestedType;
                }
            }
        }

        public static IEnumerable<Type> GetAllNestedTypesAndSelf(this Type type)
            => type.GetAllNestedTypes().Append(type);

        public static Func<object> CreateDelegate(this ConstructorInfo constructor)
        {
            Type constructorType = typeof(Func<>).MakeGenericType(constructor.DeclaringType);
            Delegate lambda = Expression.Lambda(constructorType, Expression.New(constructor)).Compile();
            return () => lambda.DynamicInvoke();
        }

        public static bool HasAttribute<T>(this MemberInfo member)
            where T : Attribute
            => member.GetCustomAttribute<T>() != null;
    }
}