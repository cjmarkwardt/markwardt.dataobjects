namespace Markwardt.DataObjects
{
    public class PropertyToken : IDataToken
    {
        public PropertyToken(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}