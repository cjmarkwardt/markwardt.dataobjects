namespace Markwardt.DataObjects
{
    public interface IValueToken : IDataToken
    {
        object Value { get; }
    }

    public class ValueToken<T> : IValueToken
    {
        public ValueToken(T value)
        {
            Value = value;
        }

        public T Value { get; }

        object IValueToken.Value => Value;
    }
}