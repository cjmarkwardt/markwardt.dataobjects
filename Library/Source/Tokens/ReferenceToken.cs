namespace Markwardt.DataObjects
{
    public class ReferenceToken : IDataToken
    {
        public ReferenceToken(string path, bool isLoaded)
        {
            Path = path;
            IsLoaded = isLoaded;
        }

        public string Path { get; }
        public bool IsLoaded { get; }
    }
}