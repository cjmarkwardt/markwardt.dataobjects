namespace Markwardt.DataObjects
{
    public class ObjectStartToken : IDataToken
    {
        public ObjectStartToken(string className)
        {
            ClassName = className;
        }

        public string ClassName { get; }

        public bool IsDictionary => string.IsNullOrWhiteSpace(ClassName);
    }
}