using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class CompositeDataSource : IDataSource
    {
        public CompositeDataSource(IDataSource defaultSource = null, string separator = ">")
        {
            DefaultSource = defaultSource;
            Separator = separator;
        }

        public IDataSource DefaultSource { get; set; }
        public string Separator { get; }

        private IDictionary<string, IDataSource> sources = new Dictionary<string, IDataSource>();

        public void Add(string key, IDataSource source)
            => sources.Add(key, source);

        public (string SourceKey, string AssetPath) ParsePath(string path)
        {
            IEnumerable<string> parts = path.Split(Separator, 2);
            if (parts.Count() == 1)
            {
                return (null, path);
            }
            else if (parts.Count() == 2)
            {
                return (parts.ElementAt(0), parts.ElementAt(1));
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public IDataSource GetSource(string sourceKey)
        {
            if (sourceKey == null)
            {
                if (DefaultSource != null)
                {
                    return DefaultSource;
                }
                else
                {
                    throw new InvalidOperationException("No default source was set");
                }
            }
            else if (sources.TryGetValue(sourceKey, out IDataSource source))
            {
                return source;
            }
            else
            {
                throw new InvalidOperationException($"Source {sourceKey} was not found.");
            }
        }

        public async Task<(bool IsLoaded, object Value)> TryLoad(IDataContext context, string path, Type type = null)
        {
            (string sourceKey, string assetPath) = ParsePath(path);
            return await GetSource(sourceKey).TryLoad(context, assetPath, type);
        }
    }
}