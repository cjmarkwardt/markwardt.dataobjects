using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface ILoadable
    {
        object Value { get; }
        bool IsLoaded { get; }

        Task Load();
        Task<object> Get();

        Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent = 0);
    }

    public interface ILoadable<T> : ILoadable
    {
        new T Value { get; }

        new Task<T> Get();
    }

    internal interface ISettableLoadable : ILoadable
    {
        void SetValue(object value);
        void SetFactory(Func<Task<object>> factory);
        void SetFactory(Func<object> factory);
        void SetPath(IDataContext context, string path);
    }

    public static class Loadable
    {
        public static ILoadable FromValue(Type type, object value)
        {
            ISettableLoadable loadable = Create(type);
            loadable.SetValue(value);
            return loadable;
        }

        public static ILoadable FromFactory(Type type, Func<Task<object>> factory)
        {
            ISettableLoadable loadable = Create(type);
            loadable.SetFactory(factory);
            return loadable;
        }

        public static ILoadable FromPath(Type type, IDataContext context, string path)
        {
            ISettableLoadable loadable = Create(type);
            loadable.SetPath(context, path);
            return loadable;
        }

        public static ILoadable FromFactory(Type type, Func<object> factory)
        {
            ISettableLoadable loadable = Create(type);
            loadable.SetFactory(factory);
            return loadable;
        }

        private static ISettableLoadable Create(Type type)
            => (ISettableLoadable) Activator.CreateInstance(typeof(Loadable<>).MakeGenericType(type), true);
    }

    public class Loadable<T> : ILoadable<T>, ISettableLoadable, IDataConvertible
    {
        private delegate Task<string> SerializeFunction(bool isFormatted, int indent = 0);

        private Loadable() { }

        public Loadable(T value)
            => SetValue(value);

        public Loadable(Func<Task<T>> factory)
            => SetFactory(async () => await factory());

        public Loadable(Func<T> factory)
            => SetFactory(() => factory());

        public Loadable(IDataContext context, string path)
            => SetPath(context, path);

        private Handler handler;

        public T Value => handler.Value;
        public bool IsLoaded => handler.IsLoaded;

        object ILoadable.Value => handler.Value;

        public async Task Load()
            => await handler.Load();

        public async Task<T> Get()
        {
            await Load();
            return Value;
        }

        async Task<object> ILoadable.Get()
            => await Get();

        public void SetValue(object value)
            => handler = new ValueHandler((T) value);

        public void SetPath(IDataContext context, string path)
            => handler = new PathHandler(context, path);

        public void SetFactory(Func<Task<object>> factory)
            => handler = new FactoryHandler(async () => (T) await factory());

        public void SetFactory(Func<object> factory)
            => handler = new FactoryHandler(() => Task.FromResult<T>((T) factory()));

        public bool CanConvert(Type type)
            => type == typeof(object) || type == typeof(T);

        public async Task<object> Convert(Type type)
        {
            if (type == typeof(object) || type == typeof(T))
            {
                return await Get();
            }

            throw new InvalidOperationException($"Cannot convert to type {type.FullName}");
        }

        public async Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent = 0)
            => await handler.Serialize(mapper, writer, isFormatted, indent);

        private abstract class Handler
        {
            private T value;
            public T Value
            {
                get => value;
                set
                {
                    if (!IsLoaded)
                    {
                        throw new InvalidOperationException("Value is not loaded");
                    }

                    this.value = value;
                }
            }

            public bool IsLoaded { get; private set; }

            private TaskCompletionSource loading;

            public async Task Load()
            {
                if (loading != null)
                {
                    await loading.Task;
                }
                else if (!IsLoaded)
                {
                    loading = new TaskCompletionSource();

                    value = await LoadValue();
                    IsLoaded = true;

                    loading.SetResult();
                    loading = null;
                }
            }

            public abstract Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent);

            protected abstract Task<T> LoadValue();
        }

        private class ValueHandler : Handler
        {
            public ValueHandler(T value)
            {
                this.value = value;
            }

            private T value;

            public override async Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent)
                => await new DataWriter(mapper).Write(value, writer, isFormatted, indent);

            protected override Task<T> LoadValue()
                => Task.FromResult(value);
        }

        private class PathHandler : Handler
        {
            public PathHandler(IDataContext context, string path)
            {
                this.context = context;
                this.path = path;
            }

            private IDataContext context;
            private string path;

            public override async Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent)
                => await writer.WriteAsync($"${path}");

            protected override async Task<T> LoadValue()
                => await context.Load<T>(path);
        }

        private class FactoryHandler : Handler
        {
            public FactoryHandler(Func<Task<T>> factory)
            {
                this.factory = factory;
            }

            private Func<Task<T>> factory;

            public override async Task Serialize(IDataMapper mapper, TextWriter writer, bool isFormatted, int indent)
            {
                await Load();
                await new DataWriter(mapper).Write(Value, writer, isFormatted, indent);
            }

            protected override async Task<T> LoadValue()
                => await factory();
        }
    }
}