using System;

namespace Markwardt.DataObjects
{
    public class StandardDataContext : DataContext
    {
        public StandardDataContext(IDataCache cache = null)
        {
            base.source = source;
            base.converter = converter;
            base.mapper = mapper;
            base.cache = cache ?? new DataCache();
        }

        private new CompositeDataSource source = new CompositeDataSource();
        private new CompositeDataConverter converter = new CompositeDataConverter();
        private new DataMapper mapper = new DataMapper();

        public IDataSource DefaultSource
        {
            get => source.DefaultSource;
            set => source.DefaultSource = value;
        }

        public void AddSource(string key, IDataSource source)
            => this.source.Add(key, source);

        public void AddConverter(IDataConverter converter)
            => this.converter.Add(converter);

        public void Map(Type type)
            => mapper.Map(type);

        public void Map<T>()
            => mapper.Map<T>();

        public void AutoMap()
            => mapper.AutoMap();

        public void AutoMapConverters()
            => converter.AutoMap();
    }
}