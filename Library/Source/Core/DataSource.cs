using System;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataSource
    {
        Task<(bool IsLoaded, object Value)> TryLoad(IDataContext context, string path, Type type = null);
    }

    public static class DataSourceExtensions
    {
        public static async Task<object> Load(this IDataSource source, IDataContext context, string path, Type type = null)
        {
            (bool isLoaded, object value) = await source.TryLoad(context, path, type);
            if (!isLoaded)
            {
                throw new InvalidOperationException($"Data value not found at path {path}");
            }

            return value;
        }
    }
}