using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataContext
    {
        Task<object> LoadStream(Stream stream, Type type = null);
        Task<object> LoadText(string text, Type type = null);
        Task<object> Load(string path, Type type = null);
        Task<(bool IsLoaded, object Value)> TryLoad(string path, Type type = null);

        Task SaveStream(object value, Stream stream, bool isFormatted = true);
        Task<string> SaveText(object value, bool isFormatted = true);
        Task Save(object value, TextWriter writer, bool isFormatted = true);

        Task Clear();
    }

    public static class DataContextUtils
    {
        public static async Task<T> LoadStream<T>(this IDataContext context, Stream stream)
            => (T) await context.LoadStream(stream, typeof(T));

        public static async Task<T> LoadText<T>(this IDataContext context, string text)
            => (T) await context.LoadText(text, typeof(T));

        public static async Task<T> Load<T>(this IDataContext context, string path)
            => (T) await context.Load(path, typeof(T));

        public static async Task<(bool IsLoaded, T Value)> TryLoad<T>(this IDataContext context, string path)
        {
            (bool isLoaded, object value) = await context.TryLoad(path, typeof(T));
            return (isLoaded, (T) value);
        }
    }

    public class DataContext : IDataContext
    {
        public DataContext(IDataSource source, IDataConverter converter, IDataMapper mapper, IDataCache cache)
        {
            this.source = source;
            this.converter = converter;
            this.mapper = mapper;
            this.cache = cache;
        }

        protected DataContext() { }

        protected IDataSource source;
        protected IDataConverter converter;
        protected IDataMapper mapper;
        protected IDataCache cache;

        public async Task<object> LoadStream(Stream stream, Type type = null)
            => await new DataReader(mapper, converter, this, stream).Read(type);

        public async Task<object> LoadText(string text, Type type = null)
            => await new DataReader(mapper, converter, this, text).Read(type);

        public async Task<object> Load(string path, Type type = null)
        {
            (bool hasValue, object value) = cache.Get(path);
            if (hasValue)
            {
                return value;
            }

            value = await source.Load(this, path, type);
            cache.Cache(path, value);
            return value;
        }

        public async Task<(bool IsLoaded, object Value)> TryLoad(string path, Type type = null)
        {
            (bool hasValue, object value) = cache.Get(path);
            if (hasValue)
            {
                return (true, value);
            }

            bool isLoaded;
            (isLoaded, value) = await source.TryLoad(this, path, type);
            if (isLoaded)
            {
                cache.Cache(path, value);
            }

            return (isLoaded, value);
        }

        public async Task SaveStream(object value, Stream stream, bool isFormatted = true)
            => await new DataWriter(mapper).WriteStream(value, stream, isFormatted);

        public async Task<string> SaveText(object value, bool isFormatted = true)
            => await new DataWriter(mapper).WriteText(value, isFormatted);

        public async Task Save(object value, TextWriter writer, bool isFormatted = true)
            => await new DataWriter(mapper).Write(value, writer, isFormatted);

        public async Task Clear()
            => await cache.Clear();
    }
}