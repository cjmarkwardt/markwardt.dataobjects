using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataCache
    {
        (bool HasValue, object Value) Get(string path);
        void Cache(string path, object value);
        Task Clear();
    }

    public class DataCache : IDataCache
    {
        protected IDictionary<string, object> values = new Dictionary<string, object>();

        public (bool HasValue, object Value) Get(string path)
        {
            if (values.TryGetValue(path, out object value))
            {
                return (true, value);
            }

            return (false, null);
        }

        public void Cache(string path, object value)
            => values[path] = value;

        public virtual async Task Clear()
        {
            foreach (object value in values.Values)
            {
                if (value is IDisposable disposable)
                {
                    disposable.Dispose();
                }
                else if (value is IAsyncDisposable asyncDisposable)
                {
                    await asyncDisposable.DisposeAsync();
                }
            }

            values.Clear();
        }
    }
}