using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Markwardt.DataObjects
{
    public interface IDataMapper
    {
        IDataTypeMap GetMap(Type type);
        IDataTypeMap GetMap(string type);
    }

    public class DataMapper : IDataMapper
    {
        private IDictionary<Type, IDataTypeMap> mapsByType = new Dictionary<Type, IDataTypeMap>();
        private IDictionary<string, IDataTypeMap> mapsByName = new Dictionary<string, IDataTypeMap>();

        public void Map(Type type, string name)
        {
            var map = new DataTypeMap(type);

            mapsByType[type] = map;
            mapsByName[name] = map;
        }

        public void Map(Type type)
        {
            Map(type, type.GetFriendlyName());
            Map(type, type.GetFullFriendlyName());
        }

        public void Map<T>()
            => Map(typeof(T));

        public void AutoMap()
        {
            foreach (Type type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).SelectMany(t => t.GetAllNestedTypesAndSelf()).Where(t => t.GetCustomAttributes<DataObjectAttribute>().Any()))
            {
                Map(type);
            }
        }

        public IDataTypeMap GetMap(string name)
        {
            if (mapsByName.TryGetValue(name, out IDataTypeMap map))
            {
                return map;
            }

            throw new InvalidOperationException($"Type name {name} was not mapped to a type");
        }

        public IDataTypeMap GetMap(Type type)
        {
            if (mapsByType.TryGetValue(type, out IDataTypeMap map))
            {
                return map;
            }

            throw new InvalidOperationException($"Type {type} was not mapped");
        }
    }
}