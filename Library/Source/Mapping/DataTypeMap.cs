using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataTypeMap
    {
        Type Type { get; }
        IReadOnlyDictionary<string, IDataPropertyMap> Properties { get; }

        object CreateInstance();

        Task OnPreSerialize(object instance);
        Task OnPostDeserialize(object instance);
    }

    public class DataTypeMap : IDataTypeMap
    {
        public DataTypeMap(Type type)
        {
            Type = type;

            constructor = () => Activator.CreateInstance(type, true);

            foreach (MethodInfo classMethod in type.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
            {
                if (classMethod.HasAttribute<OnPreSerializeAttribute>())
                {
                    onPreSerialize = CreateCallback(classMethod);
                }

                if (classMethod.HasAttribute<OnPostDeserializeAttribute>())
                {
                    onPostDeserialize = CreateCallback(classMethod);
                }
            }

            var properties = new Dictionary<string, IDataPropertyMap>();

            foreach (FieldInfo classField in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                var tag = classField.GetCustomAttribute<DataPropertyAttribute>();
                if (tag != null)
                {
                    properties.Add(tag.Name ?? classField.Name, new DataPropertyMap(classField, tag.IsOptional));
                }
            }

            foreach (PropertyInfo classProperty in type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
            {
                var tag = classProperty.GetCustomAttribute<DataPropertyAttribute>();
                if (tag != null)
                {
                    properties.Add(tag.Name ?? classProperty.Name, new DataPropertyMap(classProperty, tag.IsOptional));
                }
            }

            Properties = properties;
        }

        private Func<object> constructor;

        private Func<object, Task> onPreSerialize;
        private Func<object, Task> onPostDeserialize;

        public Type Type { get; }
        public IReadOnlyDictionary<string, IDataPropertyMap> Properties { get; }

        public object CreateInstance()
            => constructor();

        public async Task OnPreSerialize(object instance)
        {
            if (onPreSerialize != null)
            {
                await onPreSerialize.Invoke(instance);
            }
        }

        public async Task OnPostDeserialize(object instance)
        {
            if (onPostDeserialize != null)
            {
                await onPostDeserialize.Invoke(instance);
            }
        }

        private Func<object, Task> CreateCallback(MethodInfo method)
        {
            if (method.ReturnType != typeof(Task))
            {
                throw new InvalidOperationException($"Serialization callback {method.Name} must return Task");
            }

            if (method.IsStatic)
            {
                if (method.GetParameters().Count() != 1 || method.GetParameters()[0].ParameterType != Type)
                {
                    throw new InvalidOperationException($"Static serialization callback {method.Name} must take a single argument of type {Type.GetFriendlyName()}");
                }

                return instance => (Task) method.Invoke(null, new object[] { instance });
            }
            else
            {
                if (method.GetParameters().Count() != 0)
                {
                    throw new InvalidOperationException($"Instance serialization callback {method.Name} must take no arguments");
                }

                return instance => (Task) method.Invoke(instance, new object[0]);
            }
        }
    }
}