using System;

namespace Markwardt.DataObjects
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class DataObjectAttribute : Attribute { }
}