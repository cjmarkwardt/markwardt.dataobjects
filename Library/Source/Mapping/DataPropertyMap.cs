using System;
using System.Reflection;

namespace Markwardt.DataObjects
{
    public interface IDataPropertyMap
    {
        Type Type { get; }
        bool IsOptional { get; }

        void Set(object instance, object value);
    }

    public class DataPropertyMap : IDataPropertyMap
    {
        public DataPropertyMap(PropertyInfo classProperty, bool isOptional)
        {
            Type = classProperty.PropertyType;
            IsOptional = isOptional;
            setter = (instance, value) => classProperty.SetValue(instance, value);
        }

        public DataPropertyMap(FieldInfo classField, bool isOptional)
        {
            Type = classField.FieldType;
            IsOptional = isOptional;
            setter = (instance, value) => classField.SetValue(instance, value);
        }

        public Type Type { get; }
        public bool IsOptional { get; }

        private Action<object, object> setter;

        public void Set(object instance, object value)
            => setter(instance, value);
    }
}