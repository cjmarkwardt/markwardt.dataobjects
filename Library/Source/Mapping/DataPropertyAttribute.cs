using System;

namespace Markwardt.DataObjects
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class DataPropertyAttribute : Attribute
    {
        public DataPropertyAttribute(bool isOptional = false, string name = null)
        {
            IsOptional = isOptional;
            Name = name;
        }

        public string Name { get; }
        public bool IsOptional { get; }
    }
}