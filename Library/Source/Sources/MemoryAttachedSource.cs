using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class MemoryAttachedSource : IDataSource, IEnumerable<KeyValuePair<string, object>>
    {
        public MemoryAttachedSource(IDataSource source, IDictionary<string, object> instances = null)
        {
            Source = source;
            Instances = instances ?? new Dictionary<string, object>();
        }

        public IDataSource Source { get; }
        public IDictionary<string, object> Instances { get; }

        public async Task<(bool IsLoaded, object Value)> TryLoad(IDataContext context, string path, Type type = null)
        {
            if (Instances.TryGetValue(path, out object instance))
            {
                return (true, instance);
            }
            else
            {
                return await Source.TryLoad(context, path, type);
            }
        }

        public void Add(string path, object instance)
        {
            Instances.Add(path, instance);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
            => Instances.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => Instances.GetEnumerator();
    }
}