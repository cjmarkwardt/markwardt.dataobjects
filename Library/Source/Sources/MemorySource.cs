using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class MemorySource : IDataSource, IEnumerable<KeyValuePair<string, object>>
    {
        public IDictionary<string, object> Instances { get; } = new Dictionary<string, object>();

        public Task<(bool IsLoaded, object Value)> TryLoad(IDataContext context, string path, Type type = null)
        {
            if (Instances.TryGetValue(path, out object instance))
            {
                return Task.FromResult((true, instance));
            }
            else
            {
                return Task.FromResult<(bool, object)>((false, null));
            }
        }

        public void Add(string path, object instance)
        {
            Instances.Add(path, instance);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
            => Instances.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => Instances.GetEnumerator();
    }
}