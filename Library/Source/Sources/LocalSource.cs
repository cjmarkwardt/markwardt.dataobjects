using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class LocalSource : IDataSource
    {
        public LocalSource(string folder)
        {
            Folder = folder;
        }

        public string Folder { get; }

        protected virtual IDictionary<string, Loader> ExtensionLoaders { get; }

        protected virtual Loader DatvLoader { get; } = async (context, stream, type) => (true, await context.LoadStream(stream, type));

        public async Task<(bool IsLoaded, object Value)> TryLoad(IDataContext context, string path, Type type = null)
        {
            path = Path.Combine(Folder, path);

            if (File.Exists(path))
            {
                Loader loader = null;
                if (path.EndsWith(".datv"))
                {
                    loader = DatvLoader;
                }
                else
                {
                    foreach (KeyValuePair<string, Loader> extensionLoader in ExtensionLoaders)
                    {
                        if (path.EndsWith($".{extensionLoader.Key}"))
                        {
                            loader = extensionLoader.Value;
                            break;
                        }
                    }
                }

                if (loader == null)
                {
                    return (false, null);
                }

                using (Stream stream = File.OpenRead(path))
                {
                    return (true, await loader(context, stream, type));
                }
            }
            
            return (false, null);
        }

        public delegate Task<object> Loader(IDataContext context, Stream stream, Type type = null);
    }
}