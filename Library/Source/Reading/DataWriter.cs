using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class DataWriter
    {
        public DataWriter(IDataMapper mapper)
        {
            this.mapper = mapper;
        }

        private IDataMapper mapper;

        public async Task Write(object value, TextWriter writer, bool isFormatted = true, int indent = 0, bool forceSameLine = true)
        {
            string CreateIndent(int indents)
                => isFormatted ? StringUtils.CreateIndent(indents) : string.Empty;

            string space = isFormatted ? " " : string.Empty;

            if (value == null)
            {
                await writer.WriteAsync("null");
            }
            else if (value is bool boolValue)
            {
                await writer.WriteAsync(boolValue.ToString().ToLower());
            }
            else if (value is int intValue)
            {
                await writer.WriteAsync(intValue.ToString());
            }
            else if (value is float floatValue)
            {
                await writer.WriteAsync(floatValue.ToString());
            }
            else if (value is string stringValue)
            {
                await writer.WriteAsync($"\"{stringValue.ToString()}\"");
            }
            else if (value is ILoadable loadable)
            {
                await loadable.Serialize(mapper, writer, isFormatted, indent);
            }
            else if (value is IList list)
            {
                if (list.Count == 0)
                {
                    await writer.WriteAsync("[]");
                }
                else
                {
                    if (!forceSameLine && isFormatted)
                    {
                        await writer.WriteLineAsync();
                        await writer.WriteAsync($"{CreateIndent(indent)}[");
                    }
                    else
                    {
                        await writer.WriteAsync("[");
                    }

                    int count = list.Count;
                    int i = 0;
                    foreach (object item in list)
                    {
                        if (isFormatted)
                        {
                            await writer.WriteLineAsync();
                        }

                        await writer.WriteAsync($"{CreateIndent(indent + 1)}{await WriteText(item, isFormatted, indent + 1)}");

                        if (i < count - 1)
                        {
                            await writer.WriteAsync(",");
                        }

                        i++;
                    }

                    if (isFormatted)
                    {
                        await writer.WriteLineAsync();
                    }

                    await writer.WriteAsync($"{CreateIndent(indent)}]");
                }
            }
            else if (value is IDictionary dictionary)
            {
                if (dictionary.Count == 0)
                {
                    await writer.WriteAsync("{}");
                }
                else
                {
                    if (!forceSameLine && isFormatted)
                    {
                        await writer.WriteLineAsync();
                        await writer.WriteAsync($"{CreateIndent(indent)}{{");
                    }
                    else
                    {
                        await writer.WriteAsync("{");
                    }

                    int count = dictionary.Count;
                    int i = 0;
                    foreach (DictionaryEntry entry in dictionary)
                    {
                        if (isFormatted)
                        {
                            await writer.WriteLineAsync();
                        }

                        await writer.WriteAsync($"{CreateIndent(indent + 1)}{await WriteText(entry.Key)}:{space}{await WriteText(entry.Value, isFormatted, indent + 1, false)}");

                        if (i < count - 1)
                        {
                            await writer.WriteAsync(",");
                        }

                        i++;
                    }

                    if (isFormatted)
                    {
                        await writer.WriteLineAsync();
                    }

                    await writer.WriteAsync($"{CreateIndent(indent)}}}");
                }
            }
            else
            {
                Type type = value.GetType();
                IDataTypeMap typeMap = mapper.GetMap(type);
                if (type.GetCustomAttribute<DataObjectAttribute>() == null)
                {
                    throw new InvalidOperationException($"Type {type.Name} is not marked as a data object");
                }

                await typeMap.OnPreSerialize(value);

                IEnumerable<FieldInfo> classFields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(f => f.GetCustomAttribute<DataPropertyAttribute>() != null).ToList();
                IEnumerable<PropertyInfo> classProperties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(f => f.GetCustomAttribute<DataPropertyAttribute>() != null).ToList();

                if (classFields.Count() == 0 && classProperties.Count() == 0)
                {
                    await writer.WriteAsync(isFormatted ? $"{type.Name} {{}}" : $"{type.Name}{{}}");
                }
                else
                {
                    if (isFormatted)
                    {
                        await writer.WriteLineAsync($"{type.Name}");
                        await writer.WriteAsync($"{CreateIndent(indent)}{{");
                    }
                    else
                    {
                        await writer.WriteAsync($"{type.Name}{{");
                    }

                    IEnumerable<KeyValuePair<string, object>> properties = classFields.Select(f => new KeyValuePair<string, object>(f.Name, f.GetValue(value))).Concat(classProperties.Select(p => new KeyValuePair<string, object>(p.Name, p.GetValue(value)))).ToList();

                    int count = properties.Count();
                    int i = 0;
                    foreach (KeyValuePair<string, object> property in properties)
                    {
                        if (isFormatted)
                        {
                            await writer.WriteLineAsync();
                        }

                        await writer.WriteAsync($"{CreateIndent(indent + 1)}{property.Key}:{space}{await WriteText(property.Value, isFormatted, indent + 1, false)}");

                        if (i < count - 1)
                        {
                            await writer.WriteAsync(",");
                        }

                        i++;
                    }

                    if (isFormatted)
                    {
                        await writer.WriteLineAsync();
                    }

                    await writer.WriteAsync($"{CreateIndent(indent)}}}");
                }
            }
        }

        public async Task<string> WriteText(object value, bool isFormatted = true, int indent = 0, bool forceSameLine = true)
        {
            var writer = new StringWriter();
            await Write(value, writer, isFormatted, indent, forceSameLine);
            return writer.ToString();
        }

        public async Task WriteStream(object value, Stream stream, bool isFormatted = true, int indent = 0, bool forceSameLine = true)
        {
            var writer = new StreamWriter(stream);
            await Write(value, writer, isFormatted, indent, forceSameLine);
            await writer.FlushAsync();
        }
    }
}