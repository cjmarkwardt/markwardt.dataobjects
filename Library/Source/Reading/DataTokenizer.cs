using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Markwardt.DataObjects
{
    public class DataTokenizer
    {
        public DataTokenizer(TextReader reader)
        {
            this.reader = new LookAheadCharacterReader(reader);
        }

        public DataTokenizer(Stream stream)
            : this(new StreamReader(stream)) { }

        public DataTokenizer(string text)
            : this(new StringReader(text)) { }

        private LookAheadReader<char> reader;

        public string Error { get; private set; }

        public bool HasError => Error != null;

        public IEnumerable<IDataToken> Tokenize()
            => ReadValue();

        private IEnumerable<IDataToken> ReadValue()
        {
            string value = string.Empty;

            string PopValue()
            {
                string popped = value;
                value = string.Empty;
                return popped;
            }

            while (reader.MoveNext())
            {
                if (char.IsWhiteSpace(reader.Current))
                {
                    continue;
                }
                if (reader.Current == '{')
                {
                    yield return new ObjectStartToken(PopValue().Trim());
                }
                else if (reader.Current == '}')
                {
                    yield return new ObjectEndToken();
                }
                else if (reader.Current == '[')
                {
                    yield return new ListStartToken();
                }
                else if (reader.Current == ']')
                {
                    yield return new ListEndToken();
                }
                else if (reader.Current == ':')
                {
                    string property = PopValue().Trim();
                    if (!string.IsNullOrWhiteSpace(property))
                    {
                        yield return new PropertyToken(property);
                    }

                    yield return new PairSeparatorToken();
                }
                else if (reader.Current == ',')
                {
                    yield return new ItemSeparatorToken();
                }
                else if (reader.Current == '"')
                {
                    string text = string.Empty;
                    while (reader.MoveNext())
                    {
                        if (reader.Current == '\\')
                        {
                            if (!reader.MoveNext())
                            {
                                Error = "Expected escape code";
                                yield break;
                            }

                            if (reader.Current == '\\' || reader.Current == '"')
                            {
                                text += reader.Current;
                            }
                            else if (reader.Current == 'n')
                            {
                                text += Environment.NewLine;
                            }
                            else
                            {
                                Error = $"Unknown escape character {reader.Current}";
                                yield break;
                            }
                        }
                        else if (reader.Current == '"')
                        {
                            yield return new ValueToken<string>(text);
                            break;
                        }
                        else
                        {
                            text += reader.Current;
                        }
                    }
                }
                else if (reader.Current == '$')
                {
                    bool firstCharacter = true;
                    string path = string.Empty;

                    while (true)
                    {
                        if (firstCharacter)
                        {
                            firstCharacter = false;
                        }
                        else
                        {
                            reader.MoveNext();

                            if (reader.Current == '\\')
                            {
                                if (!reader.MoveNext())
                                {
                                    Error = "Expected escape code";
                                    yield break;
                                }

                                if (reader.Current == '\\' || reader.Current == '"')
                                {
                                    path += reader.Current;
                                }
                                else if (reader.Current == 'n')
                                {
                                    path += Environment.NewLine;
                                }
                                else
                                {
                                    Error = $"Unknown escape character {reader.Current}";
                                    yield break;
                                }
                            }
                            else
                            {
                                path += reader.Current;
                            }
                        }

                        if (!reader.HasNext || reader.Next == ':' || reader.Next == ',' || reader.Next == '\n' || reader.Next == '}' || reader.Next == ']')
                        {
                            bool isLoaded = true;
                            if (path.StartsWith("*"))
                            {
                                path = path.Substring(1);
                                isLoaded = false;
                            }

                            yield return new ReferenceToken(path.Trim(), isLoaded);
                            break;
                        }
                    }
                }
                else if (char.IsNumber(reader.Current))
                {
                    bool firstCharacter = true;
                    string rawNumber = reader.Current.ToString();

                    while (true)
                    {
                        if (firstCharacter)
                        {
                            firstCharacter = false;
                        }
                        else
                        {
                            reader.MoveNext();
                            rawNumber += reader.Current;
                        }

                        if (!reader.HasNext || reader.Next == ':' || reader.Next == ',' || reader.Next == '\n' || reader.Next == '}' || reader.Next == ']')
                        {
                            if (rawNumber.Contains('.'))
                            {
                                if (float.TryParse(rawNumber, out float number))
                                {
                                    yield return new ValueToken<float>(number);
                                }
                                else
                                {
                                    Error = $"Invalid float value {rawNumber}";
                                    yield break;
                                }
                            }
                            else
                            {
                                if (int.TryParse(rawNumber, out int number))
                                {
                                    yield return new ValueToken<int>(number);
                                }
                                else
                                {
                                    Error = $"Invalid int value {rawNumber}";
                                    yield break;
                                }
                            }

                            break;
                        }
                    }
                }
                else
                {
                    value += reader.Current;

                    if (value.Trim() == "false")
                    {
                        PopValue();
                        yield return new ValueToken<bool>(false);
                    }
                    else if (value.Trim() == "true")
                    {
                        PopValue();
                        yield return new ValueToken<bool>(true);
                    }
                    else if (value.Trim() == "null")
                    {
                        PopValue();
                        yield return new NullToken();
                    }
                }
            }
        }
    }
}