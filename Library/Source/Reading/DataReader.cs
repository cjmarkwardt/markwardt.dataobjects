using System;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections;

namespace Markwardt.DataObjects
{
    public class DataReader
    {
        public DataReader(IDataMapper mapper, IDataConverter converter, IDataContext context, IEnumerable<IDataToken> tokens)
        {
            this.mapper = mapper;
            this.converter = converter;
            this.context = context;
            this.reader = new LookAheadTokenReader(tokens);
        }

        public DataReader(IDataMapper mapper, IDataConverter converter, IDataContext context, TextReader reader)
            : this(mapper, converter, context, new DataTokenizer(reader).Tokenize()) { }
        
        public DataReader(IDataMapper mapper, IDataConverter converter, IDataContext context, Stream stream)
            : this(mapper, converter, context, new StreamReader(stream)) { }

        public DataReader(IDataMapper mapper, IDataConverter converter, IDataContext context, string text)
            : this(mapper, converter, context, new StringReader(text)) { }

        private IDataMapper mapper;
        private IDataConverter converter;
        private IDataContext context;
        private LookAheadReader<IDataToken> reader;

        public async Task<object> Read(Type type = null)
            => await ReadValue(type ?? typeof(object));

        private async Task<object> ReadObject(Type type, string className)
        {
            IDataTypeMap typeMap = mapper.GetMap(className);
            object instance = typeMap.CreateInstance();

            if (reader.HasNext && reader.Next is ObjectEndToken)
            {
                reader.MoveNext();

                string requiredProperty = typeMap.Properties.Where(p => !p.Value.IsOptional).Select(p => p.Key).FirstOrDefault();
                if (requiredProperty != null)
                {
                    throw new DataReadException($"Type {typeMap.Type.Name} is missing required property {requiredProperty}");
                }
            }
            else
            {
                var filledProperties = new HashSet<string>();

                while (true)
                {
                    if (!reader.MoveNext() || !(reader.Current is PropertyToken))
                    {
                        throw new DataReadException("Expected property");
                    }
                    
                    string property = ((PropertyToken)reader.Current).Name;
                    filledProperties.Add(property);

                    if (!reader.MoveNext() || !(reader.Current is PairSeparatorToken))
                    {
                        throw new DataReadException("Expected pair separator");
                    }

                    if (typeMap.Properties.TryGetValue(property, out IDataPropertyMap propertyMap))
                    {
                        try
                        {
                            propertyMap.Set(instance, await ReadValue(propertyMap.Type));
                        }
                        catch (Exception exception)
                        {
                            throw new InvalidOperationException($"Failed to set property {property} in type {typeMap.Type.GetFriendlyName()} ({className}):\n{exception}");
                        }
                    }
                    else
                    {
                        throw new DataReadException($"Unknown property {property} in type {typeMap.Type.Name} for type id {className}");
                    }

                    if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ObjectEndToken)))
                    {
                        throw new DataReadException("Expected item separator or object end");
                    }

                    if (reader.Current is ObjectEndToken)
                    {
                        break;
                    }
                }

                foreach (string requiredProperty in typeMap.Properties.Where(p => !p.Value.IsOptional).Select(p => p.Key))
                {
                    if (!filledProperties.Contains(requiredProperty))
                    {
                        throw new DataReadException($"Type {typeMap.Type.Name} is missing required property {requiredProperty}");
                    }
                }
            }

            await typeMap.OnPostDeserialize(instance);

            return instance;
        }

        private IEnumerable<IDataToken> ReadObjectTokens()
        {
            if (reader.HasNext && reader.Next is ObjectEndToken)
            {
                reader.MoveNext();
                yield return reader.Current;
                yield break;
            }

            while (true)
            {
                if (!reader.MoveNext() || !(reader.Current is PropertyToken))
                {
                    throw new DataReadException("Expected property");
                }
                
                yield return reader.Current;

                if (!reader.MoveNext() || !(reader.Current is PairSeparatorToken))
                {
                    throw new DataReadException("Expected pair separator");
                }
                
                yield return reader.Current;

                foreach (IDataToken token in ReadValueTokens())
                {
                    yield return token;
                }

                if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ObjectEndToken)))
                {
                    throw new DataReadException("Expected item separator or object end");
                }

                yield return reader.Current;

                if (reader.Current is ObjectEndToken)
                {
                    yield break;
                }
            }
        }

        private ILoadable ReadLoadableObject(Type type, string className)
        {
            IEnumerable<IDataToken> tokens = ReadObjectTokens().ToList();
            return Loadable.FromFactory(type, async () =>
            {
                var reader = new DataReader(mapper, converter, context, tokens);
                return await reader.ReadObject(type, className);
            });
        }

        private async Task<IDictionary> ReadDictionary(Type type)
        {
            Type keyType = typeof(object);
            Type valueType = typeof(object);
            if (type.IsGenericType)
            {
                Type genericType = type.GetGenericTypeDefinition();
                if (genericType == typeof(IDictionary<,>) || genericType == typeof(IReadOnlyDictionary<,>))
                {
                    keyType = type.GenericTypeArguments.ElementAt(0);
                    valueType = type.GenericTypeArguments.ElementAt(1);
                }
                else if (genericType == typeof(IEnumerable<>))
                {
                    Type enumerableType = genericType.GenericTypeArguments.First();
                    if (enumerableType.IsGenericType && enumerableType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
                    {
                        keyType = enumerableType.GenericTypeArguments.ElementAt(0);
                        valueType = enumerableType.GenericTypeArguments.ElementAt(1);
                    }
                }
            }

            var pairs = (IDictionary) Activator.CreateInstance(typeof(Dictionary<,>).MakeGenericType(keyType, valueType));

            if (reader.HasNext && reader.Next is ObjectEndToken)
            {
                reader.MoveNext();
                return pairs;
            }

            while (true)
            {
                object key = await ReadValue(keyType);

                if (!reader.MoveNext() || !(reader.Current is PairSeparatorToken))
                {
                    throw new DataReadException("Expected pair separator");
                }

                pairs.Add(key, await ReadValue(valueType));

                if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ObjectEndToken)))
                {
                    throw new DataReadException("Expected item separator or object end");
                }

                if (reader.Current is ObjectEndToken)
                {
                    return pairs;
                }
            }
        }

        private IEnumerable<IDataToken> ReadDictionaryTokens()
        {
            if (reader.HasNext && reader.Next is ObjectEndToken)
            {
                reader.MoveNext();
                yield return reader.Current;
                yield break;
            }

            while (true)
            {
                foreach (IDataToken token in ReadValueTokens())
                {
                    yield return token;
                }

                if (!reader.MoveNext() || !(reader.Current is PairSeparatorToken))
                {
                    throw new DataReadException("Expected pair separator");
                }

                yield return reader.Current;

                foreach (IDataToken token in ReadValueTokens())
                {
                    yield return token;
                }

                if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ObjectEndToken)))
                {
                    throw new DataReadException("Expected item separator or object end");
                }

                yield return reader.Current;

                if (reader.Current is ObjectEndToken)
                {
                    yield break;
                }
            }
        }

        private ILoadable ReadLoadableDictionary(Type type)
        {
            IEnumerable<IDataToken> tokens = ReadDictionaryTokens().ToList();
            return Loadable.FromFactory(type, async () =>
            {
                var reader = new DataReader(mapper, converter, context, tokens);
                return await reader.ReadDictionary(type);
            });
        }

        private async Task<IList> ReadList(Type type)
        {
            Type itemType = typeof(object);
            if (type.IsGenericType)
            {
                Type listType = type.GetGenericTypeDefinition();
                if (listType == typeof(IList<>) || listType == typeof(IReadOnlyList<>) || listType == typeof(IEnumerable<>))
                {
                    itemType = type.GenericTypeArguments.First();
                }
            }

            var items = (IList) Activator.CreateInstance(typeof(List<>).MakeGenericType(itemType));

            if (reader.HasNext && reader.Next is ListEndToken)
            {
                reader.MoveNext();
                return items;
            }

            while (true)
            {
                items.Add(await ReadValue(itemType));

                if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ListEndToken)))
                {
                    throw new DataReadException("Expected item separator or list end");
                }

                if (reader.Current is ListEndToken)
                {
                    return items;
                }
            }
        }

        private IEnumerable<IDataToken> ReadListTokens()
        {
            if (reader.HasNext && reader.Next is ListEndToken)
            {
                reader.MoveNext();
                yield return reader.Current;
                yield break;
            }

            while (true)
            {
                foreach (IDataToken token in ReadValueTokens())
                {
                    yield return token;
                }

                if (!reader.MoveNext() || (!(reader.Current is ItemSeparatorToken) && !(reader.Current is ListEndToken)))
                {
                    throw new DataReadException("Expected item separator or list end");
                }

                yield return reader.Current;

                if (reader.Current is ListEndToken)
                {
                    yield break;
                }
            }
        }

        private ILoadable ReadLoadableList(Type type)
        {
            IEnumerable<IDataToken> tokens = ReadListTokens().ToList();
            return Loadable.FromFactory(type, async () =>
            {
                var reader = new DataReader(mapper, converter, context, tokens);
                return await reader.ReadList(type);
            });
        }

        private async Task<object> ReadValue(Type type)
        {
            Type loadableType = GetLoadableType(type);

            if (!reader.MoveNext())
            {
                throw new DataReadException("Expected value");
            }
            else if (reader.Current is ObjectStartToken objectStart)
            {
                if (objectStart.IsDictionary)
                {
                    if (loadableType == null)
                    {
                        return await converter.Convert(await ReadDictionary(type), type);
                    }
                    else
                    {
                        return ReadLoadableDictionary(loadableType);
                    }
                }
                else
                {
                    if (loadableType == null)
                    {
                        return await converter.Convert(await ReadObject(type, objectStart.ClassName), type);
                    }
                    else
                    {
                        return ReadLoadableObject(loadableType, objectStart.ClassName);
                    }
                }
            }
            else if (reader.Current is ListStartToken)
            {
                if (loadableType == null)
                {
                    return await converter.Convert(await ReadList(type), type);
                }
                else
                {
                    return ReadLoadableList(loadableType);
                }
            }
            else if (reader.Current is NullToken)
            {
                return null;
            }
            else if (reader.Current is ValueToken<bool> boolValue)
            {
                return await converter.Convert(boolValue.Value, type);
            }
            else if (reader.Current is ValueToken<int> intValue)
            {
                return await converter.Convert(intValue.Value, type);
            }
            else if (reader.Current is ValueToken<float> floatValue)
            {
                return await converter.Convert(floatValue.Value, type);
            }
            else if (reader.Current is ValueToken<string> stringValue)
            {
                return await converter.Convert(stringValue.Value, type);
            }
            else if (reader.Current is ReferenceToken reference)
            {
                return await converter.Convert(Loadable.FromPath(loadableType ?? type, context, reference.Path), type);
            }
            else
            {
                throw new DataReadException($"Expected a value but got {reader.Current.GetType()}");
            }
        }

        private IEnumerable<IDataToken> ReadValueTokens()
        {
            if (!reader.MoveNext())
            {
                throw new DataReadException("Expected value");
            }
            else if (reader.Current is ObjectStartToken objectStart)
            {
                yield return reader.Current;

                if (objectStart.IsDictionary)
                {
                    foreach (IDataToken token in ReadDictionaryTokens())
                    {
                        yield return token;
                    }
                }
                else
                {
                    foreach (IDataToken token in ReadObjectTokens())
                    {
                        yield return token;
                    }
                }
            }
            else if (reader.Current is ListStartToken)
            {
                yield return reader.Current;

                foreach (IDataToken token in ReadListTokens())
                {
                    yield return token;
                }
            }
            else if (reader.Current is NullToken)
            {
                yield return reader.Current;
            }
            else if (reader.Current is ValueToken<bool> boolValue)
            {
                yield return reader.Current;
            }
            else if (reader.Current is ValueToken<int> intValue)
            {
                yield return reader.Current;
            }
            else if (reader.Current is ValueToken<float> floatValue)
            {
                yield return reader.Current;
            }
            else if (reader.Current is ValueToken<string> stringValue)
            {
                yield return reader.Current;
            }
            else if (reader.Current is ReferenceToken reference)
            {
                yield return reader.Current;
            }
            else
            {
                throw new DataReadException($"Expected a value but got {reader.Current.GetType()}");
            }
        }

        private Type GetLoadableType(Type type)
        {
            if (type != null && type.IsGenericType && type.GetGenericTypeDefinition() == typeof(ILoadable<>))
            {
                return type.GenericTypeArguments.First();
            }

            return null;
        }
    }
}