using System;

namespace Markwardt.DataObjects
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    public sealed class OnPreSerializeAttribute : Attribute { }
}