using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataConverter
    {
        IEnumerable<Type> SourceTypes { get; }

        Task<(bool IsConverted, object NewValue)> TryConvert(object value, Type newType);
    }

    public static class DataConverterUtils
    {
        public static async Task<object> Convert(this IDataConverter converter, object value, Type newType)
        {
            (bool IsConverted, object newValue) = await converter.TryConvert(value, newType);
            if (!IsConverted)
            {
                throw new InvalidOperationException($"Cannot convert type {value.GetType().GetFullFriendlyName()} to type {newType.GetFullFriendlyName()}");
            }

            return newValue;
        }
    }
}