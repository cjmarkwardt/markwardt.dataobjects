using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public abstract class SimpleDataConverter<TIn, TOut> : IDataConverter
    {
        public abstract bool TryConvert(TIn value, out TOut result);

        IEnumerable<Type> IDataConverter.SourceTypes => new Type[] { typeof(TIn) };

        Task<(bool IsConverted, object NewValue)> IDataConverter.TryConvert(object value, Type newType)
        {
            if (!typeof(TIn).IsAssignableFrom(value.GetType()) || !newType.IsAssignableFrom(typeof(TOut)) || !TryConvert((TIn) value, out TOut result))
            {
                return Task.FromResult<(bool, object)>((false, default));
            }
            
            return Task.FromResult<(bool, object)>((true, result));
        }
    }
}