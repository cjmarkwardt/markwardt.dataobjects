using System;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public interface IDataConvertible
    {
        bool CanConvert(Type type);
        Task<object> Convert(Type type);
    }
}