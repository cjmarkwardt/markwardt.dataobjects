using System;

namespace Markwardt.DataObjects
{
    public class LookAheadReader<T>
    {
        public delegate (bool HasValue, T Value) ReadFunction();

        public LookAheadReader(ReadFunction read)
        {
            this.read = read;

            (HasNext, Next) = read();
        }

        private ReadFunction read;

        public bool HasCurrent { get; private set; }
        public T Current { get; private set; }

        public bool HasNext { get; private set; }
        public T Next { get; private set; }

        public bool MoveNext()
        {
            (HasCurrent, Current) = (HasNext, Next);
            (HasNext, Next) = read();

            return HasCurrent;
        }
    }
}