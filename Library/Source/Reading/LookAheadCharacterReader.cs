using System.IO;

namespace Markwardt.DataObjects
{
    public class LookAheadCharacterReader : LookAheadReader<char>
    {
        public LookAheadCharacterReader(TextReader reader)
            : base(() =>
            {
                char? character = reader.ReadCharacter();
                return (character.HasValue, character.HasValue ? character.Value : default);
            }) { }
    }
}