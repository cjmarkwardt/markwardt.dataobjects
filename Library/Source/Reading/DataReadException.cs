using System;

namespace Markwardt.DataObjects
{
    [Serializable]
    public class DataReadException : Exception
    {
        public DataReadException(string message)
            : base(message) { }
    }
}