using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

namespace Markwardt.DataObjects
{
    public class CompositeDataConverter : IDataConverter
    {
        private IDictionary<Type, IList<IDataConverter>> converters = new Dictionary<Type, IList<IDataConverter>>();

        public IEnumerable<Type> SourceTypes => converters.Keys;

        public void AutoMap()
        {
            foreach (Type type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes()).SelectMany(t => t.GetAllNestedTypesAndSelf()).Where(t => t.GetCustomAttributes<DataConverterAttribute>().Any()))
            {
                if (!typeof(IDataConverter).IsAssignableFrom(type))
                {
                    throw new InvalidOperationException($"Type {type.GetFriendlyName()} must implement IDataConverter");
                }
                
                Add((IDataConverter) Activator.CreateInstance(type, true));
            }
        }

        public void Add(IDataConverter converter)
        {
            foreach (Type type in converter.SourceTypes)
            {
                if (!converters.TryGetValue(type, out IList<IDataConverter> relatedConverters))
                {
                    relatedConverters = new List<IDataConverter>();
                    converters.Add(type, relatedConverters);
                }

                relatedConverters.Add(converter);
            }
        }

        public async Task<(bool IsConverted, object NewValue)> TryConvert(object value, Type newType)
        {
            Type valueType = value.GetType();

            if (valueType == newType)
            {
                return (true, value);
            }
            
            if (converters.TryGetValue(newType, out IList<IDataConverter> relatedConverters))
            {
                foreach (IDataConverter converter in relatedConverters)
                {
                    (bool isConverted, object newValue) = await converter.TryConvert(value, newType);
                    if (isConverted)
                    {
                        return (true, newValue);
                    }
                }
            }
            
            if (value is IDataConvertible convertible)
            {
                if (convertible.CanConvert(newType))
                {
                    return (true, await convertible.Convert(newType));
                }
            }
            
            if (newType == typeof(object) || valueType.IsAssignableTo(newType))
            {
                return (true, value);
            }
            
            try
            {
                return (true, System.Convert.ChangeType(value, newType));
            }
            catch (InvalidCastException) { }

            return (false, default);
        }
    }
}