using System.Collections.Generic;

namespace Markwardt.DataObjects
{
    public class LookAheadTokenReader : LookAheadReader<IDataToken>
    {
        public LookAheadTokenReader(IEnumerator<IDataToken> tokens)
            : base(() =>
            {
                if (tokens.MoveNext())
                {
                    return (true, tokens.Current);
                }
                else
                {
                    return (false, default);
                }
            }) { }

        public LookAheadTokenReader(IEnumerable<IDataToken> tokens)
            : this(tokens.GetEnumerator()) { }
    }
}