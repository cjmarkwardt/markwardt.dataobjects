namespace Markwardt.DataObjects
{
    [DataObject]
    public class Dog : IAnimal
    {
        private Dog() { }

        public Dog(string name)
        {
            Name = name;
        }

        [DataProperty] public string Name { get; private set; }

        public string Sound => "bark";
    }
}