namespace Markwardt.DataObjects
{
    public interface IAnimal
    {
        string Name { get; }
        string Sound { get; }
    }
}