namespace Markwardt.DataObjects
{
    [DataObject]
    public class Record
    {
        private Record() { }

        public Record(User author, string content)
        {
            Author = author;
            Content = content;
        }

        [DataProperty] public User Author { get; private set; }
        [DataProperty] public string Content { get; private set; }

        public override string ToString()
            => Content;
    }
}