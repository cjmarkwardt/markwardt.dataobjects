using System.Drawing;

namespace Markwardt.DataObjects
{
    [DataObject]
    public class UserImageCollection
    {
        private UserImageCollection() { }

        public UserImageCollection(ILoadable<Image> avatar, Image icon)
        {
            Avatar = avatar;
            Icon = icon;
        }

        [DataProperty] public ILoadable<Image> Avatar { get; private set; }
        [DataProperty] public Image Icon { get; private set; }
    }
}