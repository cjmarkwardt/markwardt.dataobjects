namespace Markwardt.DataObjects
{
    [DataObject]
    public class Bird : IAnimal
    {
        private Bird() { }

        public Bird(string name)
        {
            Name = name;
        }

        [DataProperty] public string Name { get; private set; }

        public string Sound => "chirp";
    }
}