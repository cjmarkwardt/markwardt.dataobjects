using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    public class SampleSource : LocalSource
    {
        public SampleSource(string folder)
            : base(folder) { }

        protected override IDictionary<string, Loader> ExtensionLoaders => new Dictionary<string, Loader>()
        {
            { "png", LoadImage },
            { "jpg", LoadImage }
        };

        private Task<object> LoadImage(IDataContext context, Stream stream, Type type = null)
            => Task.FromResult<object>(new Bitmap(stream));
    }
}