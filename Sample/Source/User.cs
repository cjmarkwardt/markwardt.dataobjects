using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    [DataObject]
    public class User
    {
        [OnPreSerialize]
        private static Task OnPreSerialize(User user)
        {
            Console.WriteLine($"--((USER PRE SERIALIZE {user.Name}))--");
            return Task.CompletedTask;
        }

        private User() { }

        public User(string name, int age, ILoadable<UserImageCollection> images, IEnumerable<IAnimal> pets = null)
        {
            Name = name;
            Age = age;
            Images = images;
            Pets = pets ?? Enumerable.Empty<IAnimal>();
        }

        public User(string name, int age, ILoadable<UserImageCollection> images, params IAnimal[] pets)
            : this (name, age, images, (IEnumerable<IAnimal>)pets) { }

        [DataProperty] public string Name { get; private set; }
        [DataProperty] public int Age { get; private set; }
        [DataProperty] public ILoadable<UserImageCollection> Images { get; private set; }
        [DataProperty] public IEnumerable<IAnimal> Pets { get; private set; }

        public override string ToString()
            => Name;

        [OnPostDeserialize]
        private Task OnPostDeserialize()
        {
            Console.WriteLine($"--((USER POST DESERIALIZE {Name}))--");
            return Task.CompletedTask;
        }
    }
}