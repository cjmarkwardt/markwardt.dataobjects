﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Markwardt.DataObjects
{
    internal class Program
    {
        internal static async Task Main(string[] args)
        {
            var context = new StandardDataContext();
            context.AutoMap();
            context.AutoMapConverters();

            context.AddSource("local", new SampleSource(Path.Combine(Environment.CurrentDirectory, "Objects")));

            context.AddSource("memory", new MemorySource()
            {
                {
                    "Tom", new User
                    (
                        "Tom",
                        20,
                        new Loadable<UserImageCollection>(async () => new UserImageCollection
                        (
                            new Loadable<Image>(context, "local>Tom.png"),
                            await context.Load<Image>("local>Tom.png")
                        ))
                    )
                },
                {
                    "Bob", new User
                    (
                        "Bob",
                        52,
                        new Loadable<UserImageCollection>(async () => new UserImageCollection
                        (
                            new Loadable<Image>(context, "local>Bob.jpg"),
                            await context.Load<Image>("local>Bob.jpg")
                        )),
                        new Bird("Atlas")
                    )
                }
            });
            
            var record1 = await context.Load<Record>("local>Record1.datv");
            Console.WriteLine($"{record1.Content} (by {record1.Author})");
            
            var record2 = await context.Load<Record>("local>Record2.datv");
            Console.WriteLine($"{record2.Content} (by {record2.Author})");

            Console.WriteLine("----------------------");

            (bool isLoaded, Record record3) = await context.TryLoad<Record>("local>Record3.datv");
            Console.WriteLine(isLoaded ? "Record3.datv was loaded" : "Record3.datv was not loaded");

            Record record4;
            (isLoaded, record4) = await context.TryLoad<Record>("memory>Record4.datv");
            Console.WriteLine(isLoaded ? "Record4.datv was loaded" : "Record4.datv was not loaded");

            Console.WriteLine("----------------------");

            foreach (User user in await context.Load<UserIndex>("local>Users.datv"))
            {
                Console.WriteLine($"My name is {user.Name} and I'm {user.Age} years old");
                foreach (IAnimal animal in user.Pets)
                {
                    Console.WriteLine($"      I have a pet named {animal.Name} that goes {animal.Sound}");
                }
            }

            Console.WriteLine("----------------------");

            var tom = await context.Load<User>("memory>Tom");
            var sam = await context.Load<User>("local>Sam.datv");
            var samClone = await context.Load<User>("local>SamClone.datv");

            await tom.Images.Load();
            await sam.Images.Load();
            await samClone.Images.Load();

            await tom.Images.Value.Avatar.Load();
            await sam.Images.Value.Avatar.Load();

            Console.WriteLine($"Tom's avatar is {tom.Images.Value.Avatar.Value.Width}x{tom.Images.Value.Avatar.Value.Height}");
            Console.WriteLine($"Sam's avatar is {sam.Images.Value.Avatar.Value.Width}x{sam.Images.Value.Avatar.Value.Height}");
            Console.WriteLine($"Sam's icon is {sam.Images.Value.Icon.Width}x{sam.Images.Value.Icon.Height}");
            Console.WriteLine($"Sam's clone's icon is {samClone.Images.Value.Icon.Width}x{samClone.Images.Value.Icon.Height}");

            Console.WriteLine("----------------------");

            var images = await context.Load<UserImageIndex>("local>UserImages.datv");
            await images.Collections.Load();

            foreach (UserImageCollection collection in images.Collections.Value)
            {
                await collection.Avatar.Load();
                Console.WriteLine($"Avatar is {collection.Avatar.Value.Width}x{collection.Avatar.Value.Height} and icon is {collection.Icon.Width}x{collection.Icon.Height}");
            }

            Console.WriteLine("----------------------");

            Console.WriteLine(await context.SaveText(samClone, false));
            Console.WriteLine();
            Console.WriteLine(await context.SaveText(samClone));
        }
    }
}
