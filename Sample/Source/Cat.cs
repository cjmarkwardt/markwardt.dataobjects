namespace Markwardt.DataObjects
{
    [DataObject]
    public class Cat : IAnimal
    {
        private Cat() { }

        public Cat(string name)
        {
            Name = name;
        }

        [DataProperty] public string Name { get; private set; }

        public string Sound => "meow";
    }
}