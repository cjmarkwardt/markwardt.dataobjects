using System.Collections;
using System.Collections.Generic;

namespace Markwardt.DataObjects
{
    [DataObject]
    public class UserImageIndex
    {
        [DataProperty] public ILoadable<IEnumerable<UserImageCollection>> Collections { get; private set; }
    }
}