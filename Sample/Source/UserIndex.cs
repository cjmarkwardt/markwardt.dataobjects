using System.Collections;
using System.Collections.Generic;

namespace Markwardt.DataObjects
{
    [DataObject]
    public class UserIndex : IEnumerable<User>
    {
        [DataProperty] public IReadOnlyList<User> Users { get; private set; }

        public IEnumerator<User> GetEnumerator()
            => Users.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}